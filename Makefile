#
# makefile for sccs2rcs
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause
#

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

SOURCES = README.adoc COPYING NEWS.adoc sccs2rcs sccs2rcs.adoc Makefile control test-sccs2rcs tapview

all: sccs2rcs.1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<
clean:
	rm -f  *~ *.1 *.html *.tar.gz MANIFEST
	rm -fr .rs* typescript test/typescript

sccs2rcs-$(VERS).tar.gz: $(SOURCES) sccs2rcs.1 
	@ls $(SOURCES) sccs2rcs.1 | sed s:^:sccs2rcs-$(VERS)/: >MANIFEST
	@(cd ..; ln -s sccs2rcs sccs2rcs-$(VERS))
	(cd ..; tar -czvf sccs2rcs/sccs2rcs-$(VERS).tar.gz `cat sccs2rcs/MANIFEST`)
	@(cd ..; rm sccs2rcs-$(VERS))

pylint:
	@-pylint --score=n sccs2rcs

check:
	@$(MAKE) -s pylint
	@-shellcheck -f gcc test-sccs2rcs
	@sh test-sccs2rcs | ./tapview

version:
	@echo $(VERS)

dist: sccs2rcs-$(VERS).tar.gz

release: sccs2rcs-$(VERS).tar.gz sccs2rcs.html
	shipper version=$(VERS) | sh -e -x

refresh: sccs2rcs.html
	shipper -N -w version=$(VERS) | sh -e -x
