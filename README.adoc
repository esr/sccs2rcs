= sccs2rcs

sccs2rcs is a script to convert an existing SCCS
history into an RCS history without losing any of
the information that fits RCS's ontology.

Things to note:

  * It will NOT delete or alter your ./SCCS history under any circumstances.

  * Run in a directory where ./SCCS exists and where you can
    create ./RCS

  * Date, time, author, comments, branches, are all preserved.

This is a port to Python of a tool originally written in csh by Ken Cox
and later hacked by Brian Berliner and myself.  See the header comment
for detailed history.

Requires Python 3.

Required archaeologist's-trowel logo attribution: It's from flaticon.com.

To test sccs2rcs, do "make check".

Eric S. Raymond
esr@snark.thyrsus.com
